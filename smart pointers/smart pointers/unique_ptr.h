#pragma once
template<class T>
class unique_ptr
{

private:
   T* data;

   T& operator=(unique_ptr& data){}
   unique_ptr(unique_ptr& data){}

public:
   unique_ptr(T* data) :data(data) {
   };
   ~unique_ptr() {
      delete data;
   };

   T& operator*() const
   {
      return *data;
   };
   T* operator->() const
   {
      return data;
   };

   operator bool() const
   {
      return data != nullptr;
   }

   bool operator!() const
   {
      return !this->operator bool();
   }

};

