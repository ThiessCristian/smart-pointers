#include "unique_ptr.h"
#include "shared_ptr.h"
#include "weak_ptr.h"
#include <assert.h>

int main(){

   {
      shared_ptr<int> initialShared(new int(111));
      {
         weak_ptr<int> weakPtr = initialShared;
         shared_ptr<int> sharedFromWeak = weakPtr.lock();
         std::cout<<"value:" << *sharedFromWeak<<std::endl;
         std::cout << "use count:" << weakPtr.use_count() << std::endl;
         assert(weakPtr.use_count() == 1);

         shared_ptr<int> sharedFromWeak2 = weakPtr.lock();
         std::cout << "use count:" << weakPtr.use_count() << std::endl;
         assert(weakPtr.use_count() == 2);
      }

      std::cout << *initialShared << std::endl;
   }

   std::cout << "============================================" << std::endl;

   {
      weak_ptr<int> weakPtr;
      {
         shared_ptr<int> intPtr(new int(12));
         shared_ptr<int> intPtr2 = intPtr;

         intPtr = intPtr2;
         weakPtr = intPtr;

         shared_ptr<int> p1 = weakPtr.lock();
         shared_ptr<int> p2 = weakPtr.lock();
         shared_ptr<int> p3 = weakPtr.lock();

         std::cout << "expired: " << std::boolalpha << weakPtr.exipred()<< std::endl;
         assert(weakPtr.exipred()==false);

         std::cout << "use count:" << weakPtr.use_count() << std::endl;
         assert(weakPtr.use_count() == 3);
      }
      std::cout << "expired: " << weakPtr.exipred() << std::endl;
      assert(weakPtr.exipred() == true);

      std::cout << "use count:" << weakPtr.use_count() << std::endl;
      assert(weakPtr.use_count() == 0);

      shared_ptr<int> tryShared = weakPtr.lock();
      std::cout << "is not null:" << tryShared << std::endl;
      assert((bool)tryShared == false);
   }

   std::cout << "============================================" << std::endl;

   {
      shared_ptr<int> shared1(new int(1));
      shared_ptr<int> shared2(new int(2));

      weak_ptr<int> weak1 = shared1;
      weak_ptr<int> weak2 = shared2;

      weak1.swap(weak2);

      std::cout <<"value from first "<< *weak1.lock() << std::endl;
      std::cout <<"value from second "<< *weak2.lock() << std::endl;

      assert(*weak1.lock() == 2);
      assert(*weak2.lock() == 1);
   }
   std::cout << "============================================" << std::endl;
   {
      shared_ptr<int> shared1(new int(1));
      weak_ptr<int> weak1 = shared1;
      weak_ptr<int> weak2 = weak1;


      std::cout << "value from first " << *weak1.lock() << std::endl;
      std::cout << "value from second " << *weak2.lock() << std::endl;

   }


   return 0;
}