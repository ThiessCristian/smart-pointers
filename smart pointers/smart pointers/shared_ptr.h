#pragma once

#include <iostream>
#include <algorithm>

template<class T>
class shared_ptr
{
private:
   T* data;
   size_t* refCounter;
public:
   shared_ptr()
   {
      data = nullptr;
      refCounter = nullptr;
   };
   shared_ptr(T* obj):data(obj),refCounter(new size_t(1))
   {
      std::cout << "was created"<<std::endl;
   };
   
   shared_ptr(const shared_ptr<T>& otherShared) 
   {
      *this = otherShared;

   };
   
   ~shared_ptr()
   {
      if (refCounter != nullptr) {
         (*refCounter)--;
         std::cout << "ref was decremented" << std::endl;
         if (*refCounter == 0) {
            std::cout << "pointer was deleted" << std::endl;
            delete refCounter;
            delete data;
            //even if i delete them they still have some random data in them
            refCounter = nullptr;
            data = nullptr;
         }
      }
   };

   shared_ptr& operator=(const shared_ptr& other)
   {
      if (&other == nullptr) {
         return shared_ptr();
      }


      if (*this != other) {
         std::cout << "was copied" << std::endl;
         refCounter = other.refCounter;
         (*refCounter)++;
         data = other.data;
      }
      return *this;
   }
   bool operator!=(const shared_ptr& other) const
   {
      return !(*this == other);
   }

   bool operator==(const shared_ptr& other) const
   {
      if (&other==nullptr) {
         return false;
      }
      return other.data == data && other.refCounter == refCounter;
   }

   T& operator*() const
   {
      return *data;
   };
   T* operator->() const
   {
      return data;
   };

   size_t getRefCount() const
   {
      return *refCounter;
   };

   void swap(shared_ptr& otherPtr)
   {
      std::swap(data, otherPtr.data);
      std::swap(refCounter, otherPtr.refCounter);
   }

   operator bool() const
   {
      return data != nullptr;
   }

   bool operator!() const
   {
      return !this->operator bool();
   }
   
};

