#pragma once
template<class T>
class shared_ptr;

template<class T>
class weak_ptr
{

   shared_ptr<T>* data=nullptr;
   size_t* counter;

public:
   weak_ptr(){
      counter = new size_t(0);
   };

   weak_ptr(shared_ptr<T>& obj)
   {
      *this = obj;
   };
   weak_ptr(const weak_ptr<T>& obj)
   {
      *this = obj;
   }

   ~weak_ptr() {
      delete counter;
   };

   weak_ptr& operator=(const weak_ptr<T>& other)
   {
      counter = new size_t(*other.counter);
      data = other.data;
      return *this;
   }

   weak_ptr& operator=(shared_ptr<T>& other)
   {
      if (counter == nullptr) {
         counter = new size_t(0);
      }
      
      data = &other;
      return *this;
   }

   bool exipred() const
   {
      return !(*data);//converts to bool operator
   }

   shared_ptr<T> lock()
   {
      (*counter)++;
      return *data;
   }

   size_t use_count() const
   {
      if (this->exipred()) {
         return 0;
      }
      return *counter;
   }

   void swap(weak_ptr& otherPtr)
   {
      std::swap(data, otherPtr.data);
      std::swap(counter, otherPtr.counter);
   }

};

